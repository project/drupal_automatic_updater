<?php declare(strict_types = 1);

namespace TheTeknocat\DrupalUp\Commands\Traits;

use Psr\Log\LogLevel;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;
use TheTeknocat\DrupalUp\Commands\Command;

/**
 * Provide methods for executing external processes.
 */
trait ExecutesExternalProcesses
{
    /**
     * The path to the Ddev executable, if using ddev.
     *
     * @var string
     */
    protected $ddevExecutable = '';

    /**
     * Determine whether or not to apply git changes.
     *
     * @return bool
     */
    protected function applyGitChanges(): bool
    {
        // A trait can't implement and interface, so we have to throw an exception
        // if the class using this trait doesn't implement the method.
        throw new \Exception('You must implement the applyGitChanges() method in the class'
            . ' that is using the ExecutesExternalProcesses trait.');
    }

    /**
     * Get the command object.
     *
     * @return \TheTeknocat\DrupalUp\Commands\Command
     */
    protected function getCommandObject(): Command
    {
        // A trait can't implement and interface, so we have to throw an exception
        // if the class using this trait doesn't implement the method.
        throw new \Exception('You must implement the getCommandObject() method in the class'
            . ' that is using the ExecutesExternalProcesses trait.');
    }

    /**
     * Whether or not we are using ddev.
     *
     * @return bool
     */
    protected function usesDdev(): bool
    {
        // A trait can't implement and interface, so we have to throw an exception
        // if the class using this trait doesn't implement the method.
        throw new \Exception('You must implement the usesDdev() method in the class'
            . ' that is using the ExecutesExternalProcesses trait.');
    }

    /**
     * Get the ddev project name.
     *
     * @return string
     */
    protected function ddevProjectName(): string
    {
        // A trait can't implement and interface, so we have to throw an exception
        // if the class using this trait doesn't implement the method.
        throw new \Exception('You must implement the ddevProjectName() method in the class'
            . ' that is using the ExecutesExternalProcesses trait.');
    }

    /**
     * Run a drush command and return the process.
     *
     * Note that the --root and --yes options are automatically added.
     *
     * @param string $command
     *   The drush command to run.
     * @param array $options
     *   An array of options to pass to the command.
     * @param int $timeout
     *   The timeout for the command.
     * @param bool $streamOutput
     *   Whether to stream the output to the console. If true, the output will
     *   only be streamed to the console if using the --verbose option.
     *
     * @return \Symfony\Component\Process\Process
     *   An instance of the Symfony Process object.
     */
    protected function runDrushCommand(
        string $command,
        array $options = [],
        int $timeout = 60,
        bool $streamOutput = false
    ): Process {
        $command_array = $this->usesDdev()
            ? [$this->ddevExecutable, 'drush', $command]
            : [$this->drushPath, $command];
        $rootPath = $this->usesDdev() ? '/var/www/html' : $this->path;
        $options = array_merge(
            // Start with the drush executable and the command.
            $command_array,
            // Add the options.
            $options,
            // Add the root and yes options.
            ['--root=' . $rootPath, '--yes']
        );
        return $this->runProcess($options, $timeout, $streamOutput, $this->getCommandObject()->isDebug);
    }

    /**
     * Run a composer command and return the process.
     *
     * @param string $command
     *   The composer command to run.
     * @param array $options
     *   An array of options to pass to the command.
     * @param int $timeout
     *   The timeout for the command.
     * @param bool $streamOutput
     *   Whether to stream the output to the console. If true, the output will
     *   only be streamed to the console if using the --verbose option.
     *
     * @return \Symfony\Component\Process\Process
     *   An instance of the Symfony Process object.
     */
    protected function runComposerCommand(
        string $command,
        array $options = [],
        int $timeout = 60,
        bool $streamOutput = false
    ): Process {
        $command_array = $this->usesDdev()
            ? [$this->ddevExecutable, 'composer', $command]
            : [$this->getCommandObject()->composer(), $command];
        $rootPath = $this->usesDdev() ? '/var/www/html' : $this->path;
        $options = array_merge(
            // Start with the composer executable and the command.
            $command_array,
            // Add the options.
            $options,
            [
                // Add the working directory option.
                '--working-dir=' . $rootPath,
                // Avoid any interaction.
                '--no-interaction',
                // Use the quiet option, so it will only output on error.
                '--quiet',
            ]
        );
        return $this->runProcess($options, $timeout, $streamOutput, $this->getCommandObject()->isDebug);
    }

    /**
     * Run a git command and return the process.
     *
     * @param string $command
     *   The git command to run.
     * @param array $options
     *   An array of options to pass to the command.
     * @param int $timeout
     *   The timeout for the command.
     * @param bool $streamOutput
     *   Whether to stream the output to the console. If true, the output will
     *   only be streamed to the console if using the --verbose option.
     *
     * @return \Symfony\Component\Process\Process
     *   An instance of the Symfony Process object.
     */
    protected function runGitCommand(
        string $command,
        array $options = [],
        int $timeout = 60,
        bool $streamOutput = false
    ): Process {
        $options = array_merge(
            // Start with the git executable and the command.
            [$this->getCommandObject()->git(), $command],
            // Add the options.
            $options
        );
        if (!$this->applyGitChanges() && ($command == 'commit' || $command == 'push')) {
            $options[] = '--dry-run';
            $options[] = '-v';
        }
        $logOutput = $this->getCommandObject()->isDebug
            || (!$this->applyGitChanges()
                && ($command == 'commit'
                    || $command == 'push'
                    || ($command == 'checkout' && in_array('-b', $options))
                    || ($command == 'branch' && in_array('-D', $options))
                )
            );
        $result = $this->runProcess($options, $timeout, $streamOutput, $logOutput);
        // If we are using ddev, we need to wait until mutagen sync is complete.
        if ($this->usesDdev()) {
            $this->runProcess([$this->ddevExecutable, 'mutagen', 'sync']);
        }
        return $result;
    }

    /**
     * Run a post-update script, if any.
     *
     * @return void
     */
    public function runPostUpdateScript(): void
    {
        $post_update_script = $this->getCommandObject()->getConfig('post_update_script');
        if (!empty($post_update_script)) {
            $this->getCommandObject()->io->newLine();
            $this->getCommandObject()->info('Run post-update script:');
            $this->getCommandObject()->io->write($post_update_script . '...', false);
            // Split the command into an array.
            $script_bits = explode(' ', $post_update_script);
            $process = $this->runProcess($script_bits, 60, true, true);
            if (!$process->isSuccessful()) {
                $this->getCommandObject()->doneError();
                if (method_exists($this, 'setError')) {
                    $this->setError('Post-update script failed: ' . $process->getErrorOutput());
                }
            } else {
                $this->getCommandObject()->doneSuccess();
            }
        }
    }

    /**
     * Run a process and return the process object.
     *
     * @param array $options
     *   An array of options to pass to the command.
     * @param int $timeout
     *   The timeout for the command.
     * @param bool $streamOutput
     *   Whether to stream the output to the console. If true, the output will
     *   only be streamed to the console if using the --verbose option.
     * @param bool $logOutput
     *   Whether to log the output.
     *
     * @return \Symfony\Component\Process\Process
     *   An instance of the Symfony Process object.
     */
    protected function runProcess(
        array $options,
        int $timeout = 60,
        bool $streamOutput = false,
        bool $logOutput = false
    ): Process {
        chdir($this->path);
        $envVars = [
            'SHELL_VERBOSITY' => '0',
        ];
        if ($this->usesDdev()) {
            $envVars['COMPOSE_PROJECT_NAME'] = $this->ddevProjectName();
        }
        if ($this->getCommandObject()->isDebug) {
            $this->getCommandObject()->log(
                'Executing: ' . implode(' ', $options),
                LogLevel::DEBUG,
                true
            );
        }
        $process = new Process($options, null, $envVars);
        $process->setTimeout($timeout);
        $streamOutput = $streamOutput && $this->getCommandObject()->io->isVerbose();
        $streamedLines = [];
        $loggedLines = [];
        try {
            if ($logOutput) {
                $this->getCommandObject()->log(
                    'Command output follows...',
                    LogLevel::DEBUG,
                    true
                );
            }
            $process->run(function ($type, $data) use ($streamOutput, $logOutput, &$loggedLines, &$streamedLines) {
                $data_lines = explode(PHP_EOL, trim($data));
                foreach ($data_lines as $data_line) {
                    $data_line = rtrim($data_line);
                    if ($logOutput && !in_array($data_line, $loggedLines)) {
                        $this->getCommandObject()->log(
                            $data_line,
                            LogLevel::INFO,
                            true
                        );
                        $loggedLines[] = trim($data_line);
                    }
                    if ($streamOutput && !in_array($data_line, $streamedLines)) {
                        $this->getCommandObject()->io->text(' <fg=green>></> ' . $data_line);
                        $streamedLines[] = trim($data_line);
                    }
                }
            });
            if ($streamOutput) {
                $this->getCommandObject()->io->newLine();
            }
        } catch (\Exception $e) {
            if (method_exists($this, 'setFailed')) {
                $this->setFailed();
            }
            // Re-throw the same exception again.
            throw $e;
        }
        return $process;
    }

    /**
     * Find the drush executable.
     *
     * @return bool
     */
    protected function locateDrushExecutable(): bool
    {
        if ($this->useDdev) {
            if (empty($this->ddevExecutable)) {
                if ($this->locateDdevExecutable() === false) {
                    return false;
                }
            }
            return true;
        }
        // We expect drush to exist in the vendor/bin directory.
        $drush = $this->path . '/vendor/bin/drush';
        if (@file_exists($drush) && @is_executable($drush)) {
            $this->drushPath = $drush;
            return true;
        }
        return false;
    }

    /**
     * Locate the ddev executable.
     *
     * @return bool
     *   Whether the ddev executable was found.
     */
    protected function locateDdevExecutable(): bool
    {
        // Use Symfony ExecutableFinder to locate composer.
        $exec_finder = new ExecutableFinder();
        $this->ddevExecutable = $exec_finder->find('ddev');
        if (empty($this->ddevExecutable)) {
            return false;
        }
        return true;
    }
}
